.. post:: Nov 26, 2017
   :tags: docfactor, Docs Like Code
   :author: Author

Пост номер 2
============

Снова привет.

Добро пожаловать!



Первый абзац поста будет использоваться как краткое содержание в архивах и RSS.
О том, как настроить отображение, читайте в `Post Excerpts and Images
<Site Docfactor https://docfactor.ru>`_.

Ссылки на посты можно делать по имени файла: из разметки ``:ref:`first-post``` получается ссылка :ref:`first-post`.
Подробнее это описано в документе `Cross-Referencing Blog Pages
<http://ablog.readthedocs.org/manual/cross-referencing-blog-pages/>`_.
